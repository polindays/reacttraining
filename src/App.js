import React, { Component } from "react";
import "./App.css";
import Person from "./Person/person.js";
class App extends Component {
  state = {
    persons: [
      { id: "0df", name: "Kok", age: 20 },
      { id: "1df", name: "Lol", age: 21 },
      { id: "2df", name: "Pop", age: 22 }
    ],showPersons: false
  };
  switchClick = newName => {
    //console.log("Clicked!");
    //onClick={() => this.switchClick("Max")} too re-rendering, inefficient
    //this.switchClick.bind(this,"Bababanana")
    this.setState({
      persons: [
        { name: newName, age: 22 },
        { name: "Lol", age: 24 },
        { name: "Pop", age: 25 }
      ]
    });
  };
  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });
    const person = {
      ...this.state.persons[personIndex]
    };
    //const person=Object.assign({},this.persons[personIndex]);
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState({
      persons: persons
    });
  };
  togglePersonHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  };
  deletePersonhandler = personIndex => {
    //const persons = this.state.persons.slice();
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({ persons: persons });
  };
  render() {
    const style = {
      backgroundColor: "white",
      font: "inherit",
      border: "1px solid blue",
      padding: "8px",
      cursor: "pointer"
    };
    let persons = null;
    if (this.state.showPersons) {
      persons = (
        <div>
          {this.state.persons.map((person, index) => {
            return (
              <Person
                name={person.name}
                age={person.age}
                click={() => this.deletePersonhandler(index)}
                key={person.id}
                //if don't have key react will re-rendering whole list ineffient.
                //this ket allow react to track of individual elements which changed or not
                changed={event => this.nameChangeHandler(event, person.id)}
              />
            );
          })}
        </div>
      );
    }
    return (
      <div className="App">
        <h1>Hi, Babananana</h1>
        <button style={style} onClick={this.togglePersonHandler}>
          Toggle Persons!
        </button>
        {persons}
      </div>
    );
    //React.createElement('div',{className:'App'},React.createElement('h1',null,'Hello, it\'s me'));
  }
}

export default App;
